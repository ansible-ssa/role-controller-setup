
# Ansible Role: Red Hat Ansible Automation Platform - Automation Controller Setup

This role will install automation controller on a specified virtual machine or instance. At the moment it does not specifically test for the minimum requirements (Base OS, memory, etc.) and will fail if they aren't.

## Installation

### Prerequisites

Will install all dependencies needed for automation controller to run.

### Install

Perform the actual installation.

### Let's encrypt

Will deploy a Let's encrypt certificate to avoid browser warning messages. Make sure to point to the staging environment of Let's encrypt if you plan to redeploy often, or you will hit their rate limits.

Use the boolean "staging:true" (false by default) if you want this Role to use the staging servers.

## How to use

The role is optimized to be used from the [Playbook RHAAP](https://gitlab.com/ansible-ssa/playbook-rhaap) which will be called by a Job Template created by this role.

If you want to use it in your playbook:

```yaml
- name: Install automation controller
    include_role:
    name: ansible.ssa.general.controller_setup
```

Instead of using the role directly, consider using the [Ansible SSA Collection](https://gitlab.com/ansible-ssa/ansible-ssa-collection)

## Variables

This role is using a list of variables to configure the automation controller. Check the [defaults](defaults/main.yml) and adjust them to your needs.
